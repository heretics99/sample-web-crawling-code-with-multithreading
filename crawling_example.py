from Queue import Queue
from threading import Thread
from crawling_lib import open_url, custom_logging
import inspect, os
logging = custom_logging(logfile = inspect.getfile(inspect.currentframe()).rstrip(".py")+".log")

visited = set()
queue = Queue()
protocol = "http://"
mainurl = "www.microsoft.com/"
max_depth_level = 3
max_threads = 5
class CrawlingNested(Thread):
    """Crawling Class"""
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            params = self.queue.get() #getting object from queue
            url = params[0]
            currentlevel = params[1] + 1
            if currentlevel >= max_depth_level:
                self.queue.task_done()    
            root = open_url(url, logging)
            if root is None:
                logging.error("Could Not open url '%s'"%url)
                return
            hrefs = root.xpath("//a")
            for h in hrefs:
                try:
                    href = h.attrib['href']
                except KeyError:
                    continue
                if href.startswith("https://"):
                    continue

                if not href.startswith('http://'):
                    href = 'http://%s%s' % (mainurl, href)
                if not href.startswith('http://%s' % (mainurl)):
                    continue
                if href not in visited:
                    visited.add(href)
                    queue.put([href, currentlevel])
                    print currentlevel, href

            self.queue.task_done()


#spawning threads
for i in range(max_threads):
    t = CrawlingNested(queue)
    t.setDaemon(True)
    t.start()
queue.put([protocol+mainurl, 0])

queue.join() #wait till entire queue is processed

